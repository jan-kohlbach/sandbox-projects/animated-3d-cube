(() => {
  const radioGroup = document.querySelector('.radio-group');
  const cube = document.querySelector('.box');
  let currentClass;

  radioGroup.addEventListener('change', () => {
    const checked = radioGroup.querySelector(':checked');
    cube.classList.remove(currentClass);
    currentClass = checked.value;
    cube.classList.add(currentClass);
  });
})();
